import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import productsModule from '@/modules/products' //Importa el archivo index.js de la ruta indicada
import cartModule from '@/modules/cart'

export default new Vuex.Store({
  state: {
  },
  modules: {
    products: productsModule,
    cart: cartModule
  }
});
