import { find, filter } from 'lodash' //Importar metodo find y filter de lodash

/*
addProduct: Agregar producto al carrito
*/
export function addProduct(state, product) {
    const productInCart = find(state.cart, { id: product.id }); //Verificar que el producto no este en el carrito
    if (!productInCart) {
        const copy = Object.assign({}, product);
        copy.qty = 1; //Cantidad
        state.cart.push(copy);
    } else {
        productInCart.qty += 1;
    }
}

/*
removeProductFromCart: Remover un producto del carrito
*/
export function removeProductFromCart(state, product) {
    state.cart = filter(state.cart, ({ id }) => id !== product.id);
}