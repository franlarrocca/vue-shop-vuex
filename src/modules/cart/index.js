import state from './state' //Contiene los datos
import * as mutations from './mutations' //Modifican los datos
import * as getters from './getters' //Filtran los datos, para devolverlos

const namespaced = true; //Permite organizar los módulos

export default {
    namespaced,
    state,
    mutations,
    getters
}