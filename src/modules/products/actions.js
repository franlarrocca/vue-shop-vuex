export async function fetchProducts({commit}) {
    const data = await fetch('/fixtures/products.json');

    const products = await data.json();

    commit('products/setProducts', products, {
        root: true
    })
}

//Actions: nos permiten hacer peticiones asíncronas http.

//Parametro de fetchProducts({commit}) ???
//Hay una destructuración del objeto context: {state, commit, dispatch, rootState}

//Linea 6: commit para guardar los cambios (setear la lista de productos), se pasa como parametro un string
//que contenga el nombre del modulo y la mutacion a ejecutar. Luego el parametro que recibe la mutacion. 